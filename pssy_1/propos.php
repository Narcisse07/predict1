<?php 
session_start();  
if (!isset($_SESSION['id'])) {
    include "header2.php"; 
}
else {
    include "header.php"; 
}

?>
   <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100" style="color:white; text-align:justify; margin-left:5px; margin-right:5px;">
            Nous sommes des étudiants en 3e année au département des Technologies de l’Information et de la Communication de l’ESIGELEC. Dans le cadre de notre projet Ingénieur, nous avons décidé de réaliser un système de prédiction du cours de la bourse, qui est représenté dans le présent site web.  

        Ce site web a été créé pour vous permettre de voir les futurs cours des actions de différentes entreprises grâce à un algorithme de prédiction. Il vous permet, en plus de la prédiction, de simuler un portefeuille sélectionné suivant les critères de MARKOVITZ afin d’observer les meilleures entreprises dans lesquelles investir.  

        Grace à ce site web, vous pourrez vous créer un compte et vous connecter afin de visualiser les informations voulues en toute simplicité. Vous pourrez ensuite visualiser les tendances du marché, observer la prédiction du cours des actions de l’entreprise de votre choix et simuler la valeur potentielle de son portefeuille suivant les critères de MARKOWITZ. 
    </div>
  </section><!-- End Hero Section -->

  

<?php include "footer.php"?>