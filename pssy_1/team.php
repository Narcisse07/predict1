<?php 
session_start();  
if (!isset($_SESSION['id'])) {
    include "header2.php"; 
}
else {
    include "header.php"; 
}

?>
   <!-- ======= Hero Section ======= -->
  
    <section id="team">
  <div class="container" data-aos="fade-up">
    <div class="section-header">
      <h3 class="section-title">Team</h3>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="100">
          <div class="pic"><img src="" alt=""></div>
          <h4>AICHEOU Faroud</h4>
          <span>Member</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href=""><i class="bi bi-instagram"></i></a>
            <a href=""><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="200">
          <div class="pic"><img src="img/Ibrah_ph.png" alt=""></div>
          <h4>BA Ibrihima</h4>
          <span>Member</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href="https://www.instagram.com/ibrahimab.28/"><i class="bi bi-instagram"></i></a>
            <a href="https://www.linkedin.com/in/ibrahima-ba-6b0412188/"><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="300">
          <div class="pic"><img src="img/coulibaly_ph.png" alt=""></div>
          <h4>COULIBALY Cynthia</h4>
          <span>Member</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href=""><i class="bi bi-instagram"></i></a>
            <a href=""><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="400">
          <div class="pic"><img src="" alt=""></div>
          <h4>KOMETCHOU Chris</h4>
          <span>Member</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href=""><i class="bi bi-instagram"></i></a>
            <a href=""><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="300">
          <div class="pic"><img src="img/loicphoto.jpg" alt=""></div>
          <h4>WANDJOU Loic</h4>
          <span>Member</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href=""><i class="bi bi-instagram"></i></a>
            <a href=""><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>

      <div class="col-lg-3 col-md-6">
        <div class="member" data-aos="fade-up" data-aos-delay="300">
          <div class="pic"><img src="img/Zongo_ph.png" alt=""></div>
          <h4>ZONGO Sidi</h4>
          <span>Project Manager</span>
          <div class="social">
            <a href=""><i class="bi bi-twitter"></i></a>
            <a href=""><i class="bi bi-facebook"></i></a>
            <a href=""><i class="bi bi-instagram"></i></a>
            <a href=""><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>
    </div>

  </div>
</section><!-- End Team Section -->
    

  

<?php include "footer.php"?>