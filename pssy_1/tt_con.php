<?php
    session_start();
    include 'connect.inc.php';
    if(isset($_POST["email"]) && isset($_POST["password"])){
        $email = mysqli_real_escape_string($mysqli,$_POST["email"]);
        $password = $_POST["password"];
        $sql = "SELECT * FROM user WHERE email = '$email'";
        $run_query = mysqli_query($mysqli,$sql);
        $count = mysqli_num_rows($run_query);
        if ($count > 0) {
            $row = $run_query->fetch_assoc();
            if (password_verify($password, $row['password'])) {
                $_SESSION['name'] = $row['name'];
                $_SESSION['id'] = $row['id'];
                echo "login_successful";
                
                header("location: index.php");
                Exit();
            }else{
                echo json_encode(['status'=> 303, 'message'=> 'Login Fail']);
                Exit();
            }
        }else{
            echo json_encode(['status'=> 303, 'message'=> 'Account not created yet with this email']);
            Exit();
        }
    }else{
            echo json_encode(['status'=> 303, 'message'=> 'Empty fields']);
            Exit();
    }
 
    ?>
