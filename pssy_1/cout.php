<?php 
session_start();  
if (!isset($_SESSION['id'])) {
    include "header22.php"; 
}
else {
    include "header11.php"; 
}

?>
   <!-- ======= Hero Section ======= -->
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
      <h1>Welcome to our prediction page</h1>
      <h2>We are team of talented students doing their engineering project</h2>
      <a href="#portfolio" class="btn-get-started">Get Started</a>
    </div>
  </section><!-- End Hero Section -->

<main id="main">
  <!-- ======= Portfolio Section ======= -->
<section id="portfolio" class="portfolio">
  <div class="container" data-aos="fade-up">
    <div class="section-header">
      <h3 class="section-title">Portfolio</h3>
      <p class="section-description">Here you can see list of our portofilio</p>
    </div>

    <div class="row" data-aos="fade-up" data-aos-delay="100">
      <div class="col-lg-12 d-flex justify-content-center">
        <ul id="portfolio-flters">
          <li data-filter="*" class="filter-active">All</li>
          <li data-filter=".filter-app">Coca</li>
          <li data-filter=".filter-card">Adobe</li>
          <li data-filter=".filter-web">Apple</li>
        </ul>
      </div>
    </div>

    <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <img src="img/coca_data.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Data</h4>
          <p>Coca</p>
          <a href="img/coca_data.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="data 1"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <img src="img/apple_data.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Data</h4>
          <p>Apple</p>
          <a href="img/apple_data.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="data 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <img src="img/coca_pred.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Prediction</h4>
          <p>Coca</p>
          <a href="img/coca_pred.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="predict 1"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <img src="img/adobe_pred.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>prediction</h4>
          <p>Adobe</p>
          <a href="img/adobe_pred.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="predict 2"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <img src="img/apple_pred.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>prediction</h4>
          <p>Apple</p>
          <a href="img/apple_pred.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="predict 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-app">
        <img src="img/coca_mean.png" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Mean</h4>
          <p>Coca</p>
          <a href="img/coca_mean.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="mean 1"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <img src="img/adobe_data.PNG" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Data</h4>
          <p>Adobe</p>
          <a href="img/adobe_data.PNG" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="data 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-card">
        <img src="img/adobe_mean.png" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Mean</h4>
          <p>Adobe</p>
          <a href="img/adobe_mean.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="mean 2"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

      <div class="col-lg-4 col-md-6 portfolio-item filter-web">
        <img src="img/apple_mean.png" class="img-fluid" alt="">
        <div class="portfolio-info">
          <h4>Mean</h4>
          <p>Apple</p>
          <a href="img/apple_mean.png" data-gallery="portfolioGallery" class="portfolio-lightbox preview-link" title="mean 3"><i class="bx bx-plus"></i></a>
          <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
        </div>
      </div>

    </div>

  </div>
</section><!-- End Portfolio Section -->

</main>

  

<?php include "footer.php"?>

  