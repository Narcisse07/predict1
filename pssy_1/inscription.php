<?php 
session_start();  
    include "header3.php"; 
?>
  <section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
    <div class="container">
	<div class="row justify-content-center" style="margin:100px 0;">
		<div class="col-md-4">
			<h4 class="text-center" style="color:white;">Registration Page</h4>
			<p class="message"></p>
			<form id="register_form" method="POST" action="tt_inscript.php">
			  <div class="form-group">
			    <label for="name" style="color:white;">Full Name</label>
			    <input type="text" class="form-control" name="name" id="name" placeholder="Enter Name">
			  </div>
			  <div class="form-group">
			    <label for="email" style="color:white;">Email Address</label>
			    <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
			    
			  </div>
			  <div class="form-group">
			    <label for="password" style="color:white;">Password</label>
			    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
			  </div>
			  <div class="form-group">
			    <label for="cpassword" style="color:white;">Confirm Password</label>
			    <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Password">
			  </div>
			  <input type="hidden" name="admin_register" value="1">
			  <button type="submit" class="btn btn-primary register-btn">Register</button>
			  <!--<input style="width:100%;" value="Register" type="submit" name="register_button"class="btn btn-primary">-->
			</form>
		</div>
	</div>
</div>



    </div>
  </section><!-- End Hero Section -->


<?php
  include 'footer.php';
?>