<?php 
session_start();  

    include "header3.php"; 


?>
<section id="hero">
    <div class="hero-container" data-aos="zoom-in" data-aos-delay="100">
    

        <div class="container">
            <div class="row justify-content-center" style="margin:100px 0;">
                <div class="col-md-4">
                    <h4 class="text-center" style="color:white;">Login Page</h4>
                    <p class="message"></p>
                    <form id="login_form" method="POST" action="tt_con.php">
                    <div class="form-group">
                        <label for="email" style="color:white;">Email address</label>
                        <input type="email" class="form-control" name="email" id="email"  placeholder="Enter email">
                        
                    </div>
                    <div class="form-group">
                        <label for="password" style="color:white;">Password</label>
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                    </div>
                    <input type="hidden" name="admin_login" value="1">
                    <button type="submit" class="btn btn-success login-btn">Login</button>
                    <button type="button" class="btn btn-primary inscript-btn"><a href="inscription.php" style="color:white;">Signup</a></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
  </section><!-- End Hero Section -->
       

<?php
  include 'footer.php';
?>